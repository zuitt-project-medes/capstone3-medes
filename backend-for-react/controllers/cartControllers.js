const User = require('../models/User');

const Product = require("../models/Product");

const Order = require("../models/Order");

const Cart = require("../models/Cart");


//Route for Retrieving All Items in Cart.
module.exports.getAllItemsInMyCart = async (user) => {
	if (user.isAdmin) {
		return "Unauthorized";
	} else {
		return Cart.find({ userId: user.id }).then((cart, error) => {
			if (error) {
				return false;
			} else {
				return cart;
			}
		});
	}
};


//Route for Adding a New Item to User's Cart.
router.post('/:productId/addToCart', verify, productControllers.addToCart);

module.exports.addToCart = async (user, reqParams, reqBody) => {
	if (user.isAdmin) {
		return "Unauthorized";
	} else {
		let product = await Product.findById(reqParams.productId);

		let subTotal = reqBody.quantity * product.price;

		let newCart = new Cart({
			userId: user.id,
			product: [
				{
					productId: product.id,
					name: product.name,
					quantity: reqBody.quantity,
					price: product.price,
					src: product.src,
				},
			],
			subTotal: subTotal,
		});

		return newCart.save().then((cart, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		});
	}
};




//Route for Updating Item's Quantity.
router.put("/updateItem/:id", verify, productControllers.updateItem);

module.exports.updateItem = async (user, reqParams, reqBody) => {
	if (user.isAdmin) {
		return "Unauthorized";
	} else {
		let item = await Cart.findById(reqParams.cartId).then((data) => {
			return data;
		});
		// console.log(item.product[0].productId);
		// console.log(item.product[0].price);

		let subTotal = reqBody.quantity * item.product[0].price;

		let updatedItem = {
			product: [
				{
					productId: item.product[0].productId,
					name: item.product[0].name,
					quantity: reqBody.quantity,
					price: item.product[0].price,
					src: item.product[0].src,
				},
			],
			subTotal: subTotal,
		};
		return Cart.findByIdAndUpdate(reqParams.cartId, updatedItem).then(
			(updatedCartItem, error) => {
				if (error) {
					return false;
				} else {
					return true;
				}
			}
		);
	}
};




//Route for Deleting a Single Item.
router.delete("/deleteItem:id", verify,productControllers.deleteItem);

module.exports.deleteItem = async (user, reqParams) => {
	if (user.isAdmin) {
		return "Unauthorized";
	} else {
		return Cart.findByIdAndRemove(reqParams.cartId).then(
			(removedItem, error) => {
				if (error) {
					return false;
				} else {
					return true;
				}
			}
		);
	}
};

//Route for Deleting All Item.
router.delete("/deleteAllItem", verify,productControllers.deleteAllItem);

module.exports.deleteAllItems = async (user) => {
	if (user.isAdmin) {
		return "Unauthorized";
	} else {
		return Cart.deleteMany({ userId: user.id }).then((removedItems, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		});
	}
};
