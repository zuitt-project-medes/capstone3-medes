import { Carousel } from "react-bootstrap";

export default function Carousels() {

	const images = [
		{
			img: {
				src: "/images/slide-1.jpg",
				alt: "First slide",
			},
		},
		{
			img: {
				src: "/images/slide-2.jpg",
				alt: "Second slide",
			},
		},
		{
			img: {
				src: "/images/slide-3.jpg",
				alt: "Third slide",
			},
		},
		{
			img: {
				src: "/images/slide-4.jpg",
				alt: "Fourth slide",
			},
		},
		{
			img: {
				src: "/images/slide-5.jpg",
				alt: "Fifth slide",
			},
		},
		{
			img: {
				src: "/images/slide-6.jpg",
				alt: "Sixth slide",
			},
		},
	];

	return (
		<section id="home">
			<Carousel>
				{images.map((image, index) => {
					const { img } = image;
					return (
						<Carousel.Item key={index}>
							<img className="w-100" src={img.src} alt={img.alt} />
						</Carousel.Item>
					);
				})}
			</Carousel>
		</section>
	);
}
