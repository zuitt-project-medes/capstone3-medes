import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function Login(props){


	const {user, setUser} = useContext(UserContext);
	console.log(user)

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(true);

	function authenticate(e){
		e.preventDefault()


		fetch('http://localhost:4000/users/login', {
			method: "POST",
			headers: {
				'Content-Type' :  'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(typeof data.access !== "undefined"){

				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title: 'Login Successful',
					icon: 'success',
					text: 'Welcome to The League!'
				})
			} else {

				Swal.fire({
					title: 'Authentication Failed',
					icon: 'error',
					text: 'Please check your credentials'
				})
			}
		})

		setEmail('')
		setPassword('')

		const retrieveUserDetails = (token) => {
			fetch('http://localhost:4000/users/details', {
				method: "POST",
				headers: {
					Authorization: `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)

				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				})
			})
		}
	}


	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])

	return(
		
		(user.id !== null) ?
		
		<Navigate to= "/courses"/>
		
		:

		<Form onSubmit={e => authenticate(e)}>
		<h1>Log In</h1>
			<Form.Group controlId="email">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Insert your registered email here"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Insert your registered password here"
					value={password}
					onChange={e => setPassword(e.target.value)}
					required
				/>
			</Form.Group>

			{ isActive ?
				<Button variant="primary" type="submit" id="submitBtn" className="mt-3 mb-3">
					Log In
				</Button>
				:
				<Button variant="danger" type="submit" id="submitBtn" className="mt-3 mb-3" disabled>
					Log In
				</Button>
			}
		</Form>

	)
}
