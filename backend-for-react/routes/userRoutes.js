const express = require("express");

const router = express.Router();

const userControllers = require("../controllers/userControllers");

const auth = require("../auth");

const {verify, verifyAdmin} = auth;


//Route for Signing Up/ Registration.
router.post("/registerUser", userControllers.registerUser);


//Route for Checking if Email Exists.
router.post("/checkEmail", userControllers.checkEmail);


//Route for Log-In.
router.post("/login", userControllers.loginUser);


//Route for Retrieving User Details.
router.get("/getUserDetails/:id", verify, verifyAdmin, userControllers.getUserDetails);


// Route for Updating a Regular User to Admin.
router.put("/updateUserAsAdmin/:id", verify, verifyAdmin, userControllers.updateUserAsAdmin);


//Route for Retrieving All Users (Admin Only).
router.get("/getAllUsers", verify, verifyAdmin, userControllers.getAllUsers);


module.exports = router;
