const express = require("express");

const router = express.Router();

const productControllers = require("../controllers/productControllers");

const auth = require("../auth");

const {verify, verifyAdmin} = auth;


//Route for Adding a New Product.
router.post('/addProduct', verify, verifyAdmin, productControllers.addProduct);


//Route for Retrieving All Products.
router.get("/getAllProducts", productControllers.getAllProducts);


//Route for Retrieving All Active Products.
router.get('/getActiveProducts', productControllers.getActiveProducts);


//Route for Retrieving a Specific Product.
router.get("/getSingleProduct/:id", productControllers.getSingleProduct);


//Route for Updating a Product.
router.put("/updateProduct/:id", verify, verifyAdmin, productControllers.updateProduct);


//Route for Archiving a Product.
router.put("/archive/:id", verify, verifyAdmin, productControllers.archiveProduct);


//Route for Activating a Product.
router.put("/activate/:id", verify, verifyAdmin, productControllers.activateProduct);


//Route for Deleting a Product.
router.delete("/delete/:id", verify, verifyAdmin, productControllers.deleteProduct);


module.exports = router;
