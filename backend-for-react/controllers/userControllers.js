const User = require("../models/User");

const bcrypt = require("bcrypt");

const auth = require("../auth");


//For Signing Up/ Registration.
module.exports.registerUser = (req, res) => {

	User.find({})
	.then(users => {

		let searchUserName = users.filter(user => {
			return user['userName'] === req.body.userName
		})

		if(searchUserName.length === 0){

			const hashedPW = bcrypt.hashSync(req.body.password, 10)

		let newUser = new User({

		userName: req.body.userName,
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPW,
		mobileNo: req.body.mobileNo

		})
	
			newUser.save();
			res.send(newUser);
		} else {
			return res.status().send('The Username is already used. Please try again.')
		}
	})
	.catch(err => res.send(err));
}


//For Checking if Email Exists.
module.exports.checkEmail = (req,res) => {
	User.findOne({email: req.body.email})
	.then(result => {
		if(result === null){
			return res.send("Email is available.");
		} else {
			return res.send("Email is already registered.")
		}
	})
	.catch(err => res.send(err));
};


//For Log-In.
module.exports.loginUser = (req, res) => {
	console.log(req.body);
	User.findOne({email: req.body.email})
		.then(foundUser => {
			if(foundUser === null){
				return res.send("User does not exist");
			} else {
				const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)
					if(isPasswordCorrect){
			 			return res.send({accessToken: auth.createAccessToken(foundUser)})
					} else {
			 			return res.send("Password is incorrect.")
			 		}
			}
		})
		.catch(err => res.send(err));
};


//For Retrieving User Details.
module.exports.getUserDetails = (req, res) => {
	console.log(req.user)
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
};


//For Updating a Regular User to Admin.
module.exports.updateUserAsAdmin = (req, res) => {
	console.log(req.user.id);
	console.log(req.params.id);
		let updates = {
			isAdmin: true
		};
	User.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));
};



//For Retrieving All Users (Admin Only).
module.exports.getAllUsers = (req, res) => {
	User.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

