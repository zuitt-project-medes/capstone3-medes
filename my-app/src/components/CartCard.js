import { Fragment, useContext, useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import Swal from "sweetalert2";
import UserContext from "../pages/UserContext";
import { IoCloseOutline } from "react-icons/io5";

export default function CartCard(props) {

	useEffect(() => {
	}, []);

	const { setBadgeCount, setTotalAmount } = useContext(UserContext);
	const [newQuantity, setNewQuantity] = useState(props.quantity);
	const [newSubTotal, setNewSubTotal] = useState(props.subtotal);

	const decrease = () => {
		if (newQuantity > 1) {
			setNewQuantity(newQuantity - 1);
			setNewSubTotal((newQuantity - 1) * props.price);
		}
	};

	const increase = () => {
		setNewQuantity(newQuantity + 1);
		setNewSubTotal((newQuantity + 1) * props.price);
	};

	const removeItem = () => {
		fetch(`https://calm-brook-85405.herokuapp.com/carts/${props.id}/delete`, {
			method: "DELETE",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
		})
			.then((res) => res.json())
			.then((data) => {
				// console.log(data);
				if (data === true) {
					setBadgeCount((prevCount) => Math.max(prevCount - 1, 0));
					Swal.fire({
						title: "Item removed from cart",
						icon: "success",
					});
				} else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again",
					});
				}
			});
	};

	const updateItemQuantity = () => {
		fetch(`https://calm-brook-85405.herokuapp.com/carts/${props.id}/update`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
			body: JSON.stringify({
				quantity: newQuantity,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				// console.log(data);
			});
	};

	useEffect(() => {
		updateItemQuantity();
	}, [newQuantity]);

	return (
		<Fragment>
			<div className="cart-item mb-4">
				<div className="row">
					<div className="col-5 col-lg-3">
						<img className="w-100 pl-3" src={props.src} alt=""></img>
					</div>
					<div className="col-7 col-lg-9">
						<div className="d-flex justify-content-between">
							<div>{props.name}</div>
							<Button
								variant="light"
								onClick={removeItem}
								className="rounded-0"
							>
								<IoCloseOutline className="text-danger" size={25} />
							</Button>
						</div>
						<div className="mb-3">
							<div className="text-muted">₱{props.price.toLocaleString()}</div>
						</div>
						<p>Quantity:</p>
						<div className="mb-2">
							<div className="d-flex justify-content-start">
								<Button
									onClick={decrease}
									variant="outline-dark"
									className="rounded-0"
								>
									-
								</Button>
								<span className="quantity">{newQuantity}</span>
								<Button
									onClick={increase}
									variant="outline-dark"
									className="rounded-0"
								>
									+
								</Button>
							</div>
						</div>
						<div>
							<div className="d-flex justify-content-start">
								Subtotal: ₱{newSubTotal.toLocaleString()}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div className="border-top mb-4"></div>
		</Fragment>
	);
}
