const { prependListener } = require("../models/Order");

const User = require("../models/User");

const Product = require("../models/Product");

const Order = require("../models/Order");

const Cart = require("../models/Cart");


// For Checking-Out Orders
module.exports.createOrder = async (req, res) => {

	console.log(req.user.id);
	console.log(req.body.productId);
	console.log(req.body.totalAmount);

	if (req.user.isAdmin) {
		return res.send("Action Forbidden for Admin.");
	};

	let isUserUpdated = await User.findById(req.user.id)
	.then(user => {

		console.log(user);

		let newOrder = {
			userId : req.user.id,
			productId : req.body.productId,
			totalAmount : req.body.totalAmount
		};
		user.orders.push(newOrder);

		// save
		return user.save()
		.then(user => true)
		.catch(err => err.message);
	});

	if (isUserUpdated !== true) {
		return res.send({message : isUserUpdated});
	}

	let isProductUpdated = await Product.findById(req.body.productId)
	.then(product => {

		console.log(product);

		let orderer = {
			userId : req.user.id,
			productId : req.body.productId,
			totalAmount : req.body.totalAmount
		};
		product.orders.push(orderer);

		return product.save()
		.then(product => true)
		.catch(err => err.message);
	});

	if (isProductUpdated !== true) {
		return res.send({message : isProductUpdated});
	}

	if (isUserUpdated && isProductUpdated) {
		return res.send({message : 'Order Successful!'});
	}
};

//For Getting User's Orders.
module.exports.getMyOrders = (req, res) => {

	User.findById(req.user.id)
	.then(user => {
		console.log(user);
		return res.send(user.orders);
	})
	.catch(err => res.send(err));
};


//For Getting All Orders (Admin Only)
module.exports.getAllOrders = (req, res) => {

	User.find({})
	.then(allUsers => {

		// log info to check
		console.log(allUsers);
		console.log(allUsers.length);

		allOrders = []

		for (let i = 0; i < allUsers.length; i++) {
			console.log(`\n${i}\n`);
			console.log(allUsers[i]);
			if (allUsers[i].orders.length > 0) {
				allOrders.push(allUsers[i].email);
				allOrders.push(allUsers[i].orders);
			}
		};
		
		res.send(allOrders);
	})
	.catch(err => res.send(err));
};
