const express = require("express");

const router = express.Router();

const cartController = require("../controllers/cartControllers");

const auth = require("../auth");

const {verify, verifyAdmin} = auth;


//Route for Retrieving All Items in Cart.
router.get("/", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization);

	cartController
		.getAllItemsInMyCart(user)
		.then((resultFromController) => res.send(resultFromController));
});


//Route for Adding a New Item to User's Cart.
router.post("/:productId/addToCart", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization);

	cartController
		.addToCart(user, req.params, req.body)
		.then((resultFromController) => res.send(resultFromController));
});


//Route for Updating Item's Quantity.
router.put("/:cartId/update", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization);
	cartController
		.updateItem(user, req.params, req.body)
		.then((resultFromController) => res.send(resultFromController));
});


//Route for Deleting a Single Item.
router.delete("/deleteItem:id", verify,productControllers.deleteItem);
router.delete("/:cartId/delete", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization);

	cartController
		.deleteItem(user, req.params)
		.then((resultFromController) => res.send(resultFromController));
});


//Route for Deleting All Item.
router.delete("/deleteAll", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization);

	cartController
		.deleteAllItems(user)
		.then((resultFromController) => res.send(resultFromController));
});

module.exports = router;
