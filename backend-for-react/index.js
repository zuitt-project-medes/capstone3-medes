const express = require('express');

const mongoose = require('mongoose');

const cors = require('cors');

const userRoutes = require('./routes/userRoutes');

const productRoutes = require('./routes/productRoutes');

const orderRoutes = require('./routes/orderRoutes');

const cartRoutes = require('./routes/cartRoutes');

const port = process.env.port || 4000;

const app = express();


mongoose.connect("mongodb+srv://admin_medes:admin169@cluster0.ovov6.mongodb.net/Capstone3API?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
});


let db = mongoose.connection;


db.on('error', console.error.bind(console, 'Connection Error'));

db.once('open', () => console.log('Connected to MongoDB'));


app.use(express.json());

app.use(cors());

app.use('/users', userRoutes);

app.use('/products', productRoutes);

app.use('/orders', orderRoutes);

app.use('/carts', cartRoutes);

app.listen(port, () => console.log(`Server is running at port ${port}`));

