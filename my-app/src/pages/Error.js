import React, {useState} from 'react'
import {Link} from 'react-router-dom'
import {Row, Col, Button} from 'react-bootstrap';

export default function Error() {
	return (

		<Row className="min-vh-100 row justify-content-center py-5 text-center">
			<Col className="col-md-6 error-page">

				<h1 className="mb-5 big-heading">Error 404: PAGE NOT FOUND</h1>

				<h4 className="mb-5>The page you are trying to access is not available.</h4>

				<h5 className="mb-5">
					<Link className="link-dark" as={Link} to="/">Return to Homepage
					</Link>
				</h5>
			</Col>
		</Row>
	)
};
