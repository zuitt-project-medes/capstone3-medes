const express = require("express");

const router = express.Router();

const orderControllers = require("../controllers/orderControllers");

const auth = require("../auth");

const {verify, verifyAdmin} = auth;


//Route for Creating an Order (Regular User Only).
router.post("/checkOut", verify, userControllers.checkOut);


//Route for Getting User's Orders.
router.get("/getMyOrders", verify, verifyAdmin, userControllers.getMyOrders);


// Get All Orders (Admin Only)
router.get("/getAllOrders", verify, verifyAdmin, userControllers.getAllOrders);


module.exports = router;
