const Product = require("../models/Product");

//For Adding a New Product.
module.exports.addProduct = (req, res) => {
	console.log(req.body);

	Product.findOne({name : req.body.name})

	.then(result => {

		if (result !== null && result.name === req.body.name) {
			return res.send('Product Name is already registered, please try a different product.');

		} else {

			if (req.body.publishedOn !== undefined) {
				let newProduct = new Product({
					name : req.body.name,
					description : req.body.description,
					price : req.body.price,
					publishedOn : new Date(req.body.publishedOn)
				});

				newProduct.save()
				.then(product => res.send(product))
				.catch(err => res.send(err));

			} else {
				let newProduct = new Product({
					name : req.body.name,
					description : req.body.description,
					price : req.body.price
				});

				newProduct.save()
				.then(product => res.send(product))
				.catch(err => res.send(err));
			}
		};
	})
	.catch(err => res.send(err));
};


//For Retrieving All Products.
module.exports.getAllProducts = (req, res) => {

	Product.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};


//For Retrieving All Active Products.
module.exports.getActiveProducts = (req,res) => {

	Product.find({isActive: true})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};


//For Retrieving a Specific Product.
module.exports.getSingleProduct = (req, res) => {

	Product.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
};


//For Updating a Product.
module.exports.updateProduct = (req, res) => {

	let updates = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price		
	}

	Product.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(err => res.send(err));
};


//For Archiving a Product.
module.exports.archiveProduct = (req, res) => {

	let updates = {
		isActive: false
	}
	Product.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(error => res.send(error))
};


//For Activating a Product
module.exports.activateProduct = (req, res) => {

	let updates = {
		isActive: true
	}
	Product.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(error => res.send(error))
};


//For Deleting a Product.
module.exports.deleteProduct = (req, res) => {
	console.log(req.params.id)
	Product.deleteOne({_id: req.params.id})
	.then(result => res.send(result))
	.catch(error => res.send(error))
};








