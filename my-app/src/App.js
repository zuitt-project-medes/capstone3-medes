import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import AppNavBar from './components/AppNavBar';
import AppFooter from "./components/AppFooter";


import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Shop from './pages/Shop';


import ProductView from './components/ProductView';
import CartView from './components/CartView';
import OrderView from './components/OrderView';
import AdminOrdersView from './pages/AdminOrdersView';
import PlaceholderTest from "./pages/Placeholder";
import Error from './pages/Error';


import './App.css';
import { UserProvider } from './pages/UserContext'
import "bootstrap/dist/css/bootstrap.min.css";


function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
    firstName: null
  });

  const [badgeCount, setBadgeCount] = useState(0);
  let badge = [];

  const [totalAmount, setTotalAmount] = useState(0);


  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    fetch('http://localhost:4000/users/details',{
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)

      if(typeof data._id !== "undefined"){

        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
          firstName: data.firstName
        });
      } else {
        setUser({
          id: null,
          isAdmin: null,
          firstName: null
        });
      }
    });

  }, [user.firstName]);

const getUserCart = () => {
    fetch(`http://localhost:4000/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setBadgeCount(data.length);
      });
  };

  useEffect(() => {
    getUserCart();
  }, [badge]);

  return (
    <UserProvider value={{user, setUser, unsetUser, badgeCount, setBadgeCount, totalAmount, setTotalAmount}}>
      <Router>
        <AppNavBar/>
        <Container>
        <Routes>

          <Route exact path= "/" element={<Home/>}/>

          <Route exact path="/login" element={<Login/>}/>
          
          <Route exact path="/logout" element={<Logout/>}/>

          <Route exact path="/Register" element={<Register/>}/>

          <Route exact path="/shop" element={<Shop/>}/>

          <Route exact path="/products/:productId" element={<ProductView/>}/>

          <Route exact path="/cart" element={<CartView/>}/>

          <Route exact path="/orders" element={<OrderView/>}/>

          <Route exact path="/admin/orders" element={<AdminOrdersView/>}/>

          <Route exact path="/placeholder" element={<PlaceholderTest/>}/>

          <Route path="*" element={<Error />} />
        </Routes>
      <AppFooter/>
      </Container>
    </Router>
    </UserProvider>
  )
};

export default App;
