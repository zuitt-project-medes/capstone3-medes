import { Fragment, useContext } from 'react';
import UserContext from "./UserContext";
import AdminDashboard from "./AdminDashboard";

export default function Home() {
	const { user } = useContext(UserContext);

	return !user.isAdmin ? (
		<Fragment>
			// <Carousels />
			// <New Arrivals />
			// <Collections />
			// <On Sale />
		</Fragment>
	) : (
		<AdminDashboard />
	);
}