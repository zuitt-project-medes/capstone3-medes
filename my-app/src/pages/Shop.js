import { Fragment, useState, useEffect, useContext } from "react";
import { Breadcrumb, Container, Col, Row } from "react-bootstrap";
import Swal from "sweetalert2";
import { Link } from "react-router-dom";
import ProductCard from "../components/ProductCard";
import UserContext from "./UserContext";

export default function Shop() {

	const { user } = useContext(UserContext);
	const [products, setProducts] = useState([]);
	const [isLoading, setIsLoading] = useState(true);
	const [initialRun, setInitialRun] = useState(true);

	const timer = () => {
		let timerInterval;
		Swal.fire({
			title: "Loading...",
			timer: 500,
			timerProgressBar: true,
			didOpen: () => {
				Swal.showLoading();
			},
			willClose: () => {
				clearInterval(timerInterval);
			},
		});
	};

	useEffect(() => {
		fetch("https://calm-brook-85405.herokuapp.com/products")
			.then((res) => res.json())
			.then((data) => {
				// console.log(data);
				setProducts(
					data.reverse().map((product) => {
						return (
							<Col xs={6} lg={3} className="mb-2 mb-lg-4" key={product._id}>
								<ProductCard
									id={product._id}
									name={product.name}
									price={product.price.toLocaleString()}
									src={product.src}
								/>
							</Col>
						);
					})
				);
				setIsLoading(false);
			});
		if (initialRun) {
			setInitialRun(false);
			timer();
		}
	}, [initialRun]);

	return (
		<Fragment>
			<section id="shop-all">
				<Container fluid className="min-vh-100 padding-x py-5">
					{user.isAdmin ? (
						<Fragment>
							<Breadcrumb className="px-lg-3">
								<Breadcrumb.Item>
									<Link to="/" className="text-dark">
										HOME
									</Link>
								</Breadcrumb.Item>

								<Breadcrumb.Item active>ALL ACTIVE PRODUCTS</Breadcrumb.Item>
							</Breadcrumb>
							<h1 className="big-heading px-lg-3 mt-4 mb-5">
								ALL ACTIVE PRODUCTS
							</h1>
						</Fragment>
					) : (
						<Fragment>
							<Breadcrumb className="px-lg-3">
								<Breadcrumb.Item>
									<Link to="/" className="text-dark">
										HOME
									</Link>
								</Breadcrumb.Item>

								<Breadcrumb.Item active>SHOP ALL</Breadcrumb.Item>
							</Breadcrumb>
							<h1 className="big-heading px-lg-3 mt-4 mb-5">SHOP ALL</h1>
						</Fragment>
					)}

					{isLoading ? (
						<p className="ms-lg-3">Loading...</p>
					) : (
						<Fragment>
							<Row className="px-lg-3">{products}</Row>
						</Fragment>
					)}
				</Container>
			</section>
		</Fragment>
	);
}
